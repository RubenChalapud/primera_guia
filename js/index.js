$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000 
    });
    $('#contacto').on('show.bs.modal', function(e){
        console.log('Se esta mostrando el modal');
        $('#contactoBtn').removeClass('btn-outine-success');
        $('#contactoBtn').addClass('btn-dark');

    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('Se mostró el modal');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('Se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('Se ocultó');
    });
});